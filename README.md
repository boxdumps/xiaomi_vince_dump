## vince-user 8.1.0 OPM1.171019.019 V11.0.2.0.OEGMIXM release-keys
- Manufacturer: xiaomi
- Platform: msm8953
- Codename: vince
- Brand: Xiaomi
- Flavor: vince-user
- Release Version: 12
- Id: SQ1D.220205.003
- Incremental: eng.cirrus.20220326.095900
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SQ1D.220205.003/8069835:user/release-keys
- OTA version: 
- Branch: vince-user-8.1.0-OPM1.171019.019-V11.0.2.0.OEGMIXM-release-keys-random-text-3932742117249
- Repo: xiaomi_vince_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
